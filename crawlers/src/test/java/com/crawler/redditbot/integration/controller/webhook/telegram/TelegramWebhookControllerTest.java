package com.crawler.redditbot.integration.controller.webhook.telegram;

import com.crawler.redditbot.controller.webhook.telegram.dto.TelegramWebhookRequest;
import com.crawler.redditbot.domain.service.SubRedditCrawler;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.http.HttpStatus;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;

import static com.jayway.restassured.RestAssured.given;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
//@AutoConfigureWireMock(port=18737, stubs="classpath:/clients/stubs")
public class TelegramWebhookControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void createMessage() throws Exception {
        String jsonPath = ClassLoader.getSystemResource("payloads/telegram_webhook_message_payload.json").getFile();

        TelegramWebhookRequest telegramWebhookRequest = objectMapper.readValue(new File(jsonPath), TelegramWebhookRequest.class);
        String telegramWebhookRequestJson = objectMapper.writeValueAsString(telegramWebhookRequest);

        given()
                .when()
                .header("Content-Type", "application/json")
                .body(telegramWebhookRequestJson)
                .post(String.format("http://localhost:%s/api/webhooks/telegram", port))
                .then()
                .header("Content-Type", "application/json")
                .statusCode(HttpStatus.SC_NO_CONTENT);

    }

}