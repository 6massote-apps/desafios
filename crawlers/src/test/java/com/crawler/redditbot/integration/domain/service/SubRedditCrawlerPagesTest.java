package com.crawler.redditbot.integration.domain.service;

import com.crawler.redditbot.domain.service.SubRedditCrawler;
import com.crawler.redditbot.domain.model.SubRedditThread;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

@ActiveProfiles("crawlerpages")
@RunWith(SpringRunner.class)
@SpringBootTest
public class SubRedditCrawlerPagesTest {

    @Autowired
    private SubRedditCrawler subRedditCrawler;

    @Test
    public void givenSubReddit_processNPages() {
        final SubRedditThread subRedditThread = subRedditCrawler
                .process("cats");

        assertNotNull(subRedditThread);
    }

    @Test
    public void givenSubRedditsNotFound_process() {
        final SubRedditThread subRedditThread = subRedditCrawler
                .process("notfound");

        assertNull(subRedditThread);
    }

}