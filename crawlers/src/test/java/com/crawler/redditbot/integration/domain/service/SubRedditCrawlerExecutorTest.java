package com.crawler.redditbot.integration.domain.service;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.crawler.redditbot.domain.notifier.TelegramNotifier;
import com.crawler.redditbot.domain.service.SubRedditCrawlerExecutor;
import com.crawler.redditbot.domain.model.NotificationRequirements;
import com.crawler.redditbot.domain.model.SearchRequirements;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureWireMock(port=18737, stubs="classpath:/clients/stubs")
public class SubRedditCrawlerExecutorTest {

    @Mock
    private TelegramNotifier telegramNotifierMock;

    @Autowired
    @InjectMocks
    private SubRedditCrawlerExecutor subRedditCrawlerExecutor;

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("com.crawler.redditbot");
    }

    @Test
    public void execute() {
        SearchRequirements searchRequirements =  Fixture.from(SearchRequirements.class).gimme("new");
        NotificationRequirements notificationRequirements =  Fixture.from(NotificationRequirements.class).gimme("new");

        subRedditCrawlerExecutor.executeSync(searchRequirements, notificationRequirements);
    }

    @Test
    public void executeAsync() {
        SearchRequirements searchRequirements =  Fixture.from(SearchRequirements.class).gimme("async");
        NotificationRequirements notificationRequirements =  Fixture.from(NotificationRequirements.class).gimme("new");

        subRedditCrawlerExecutor.executeAsync(searchRequirements, notificationRequirements);
    }

}
