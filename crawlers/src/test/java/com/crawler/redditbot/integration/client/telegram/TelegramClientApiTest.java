package com.crawler.redditbot.integration.client.telegram;

import com.crawler.redditbot.client.telegram.TelegramClientApi;
import com.crawler.redditbot.client.telegram.TelegramMessagePayload;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureWireMock(port=18737, stubs="classpath:/clients/stubs")
public class TelegramClientApiTest {

    @Autowired
    private TelegramClientApi telegramClientApi;

    @Test
    public void bean() {
        assertThat(telegramClientApi).isInstanceOfAny(TelegramClientApi.class);
        assertThat(telegramClientApi).isNotNull();
    }

    @Test
    public void givenSendMessageRequest_returnWithSuccess() {
        TelegramMessagePayload telegramMessagePayload = TelegramMessagePayload.builder()
                .chat_id(476304284)
                .text("<b>Programming trend: </b> <a href=\"http://www.example.com/\">This is Potato. She doesnt</a>, <b>SPFC trend: </b> <a href=\"http://www.example.com/\">This is Potato. She doesnt</a>")
                .parse_mode("HTML")
                .build();
        telegramClientApi.sendMessage(telegramMessagePayload);
    }

    @Test(expected = feign.FeignException.class)
    public void givenSendMessageRequest_returnWithFailure() {
        TelegramMessagePayload telegramMessagePayload = TelegramMessagePayload.builder()
                .chat_id(1000000)
                .text("<b>Programming trend: </b> <a href=\"http://www.example.com/\">This is Potato. She doesnt</a>, <b>SPFC trend: </b> <a href=\"http://www.example.com/\">This is Potato. She doesnt</a>")
                .parse_mode("HTML")
                .build();

        telegramClientApi.sendMessage(telegramMessagePayload);
    }

}
