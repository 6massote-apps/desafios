package com.crawler.redditbot.integration.domain.service;

import com.crawler.redditbot.domain.service.SubRedditCrawler;
import com.crawler.redditbot.domain.model.SubRedditThread;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.wiremock.AutoConfigureWireMock;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Map;

import static org.junit.Assert.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureWireMock(port=18737, stubs="classpath:/clients/stubs")
public class SubRedditCrawlerTest {

    @Autowired
    private SubRedditCrawler subRedditCrawler;

    @Test
    public void givenSubReddit_process() {
        final SubRedditThread subRedditThreads = subRedditCrawler
                .process("cats");

        assertNotNull(subRedditThreads);
    }

    @Test
    public void givenSubRedditsNotFound_process() {
        final SubRedditThread subRedditThreads = subRedditCrawler
                .process("notfound");

        assertNull(subRedditThreads);
    }

}