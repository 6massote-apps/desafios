package com.crawler.redditbot.contract.api.webhook.telegram;

import com.crawler.redditbot.controller.webhook.telegram.dto.TelegramWebhookRequest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.json.AutoConfigureJson;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@AutoConfigureJson
public class WebHookTelegramRequestTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void convertFromJsonToMessage() throws IOException, URISyntaxException {
        String jsonPath = ClassLoader.getSystemResource("payloads/telegram_webhook_message_payload.json").getFile();

        TelegramWebhookRequest telegramWebhookRequest = objectMapper.readValue(new File(jsonPath), TelegramWebhookRequest.class);

        assertThat(telegramWebhookRequest).isInstanceOfAny(TelegramWebhookRequest.class);
        assertThat(telegramWebhookRequest).isNotNull();
        assertThat(telegramWebhookRequest.isMessage()).isTrue();
    }

}
