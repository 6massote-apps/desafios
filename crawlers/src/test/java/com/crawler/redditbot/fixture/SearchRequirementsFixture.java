package com.crawler.redditbot.fixture;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.crawler.redditbot.domain.model.SearchActionType;
import com.crawler.redditbot.domain.model.SearchRequirements;
import com.crawler.redditbot.domain.model.SubRedditCrawlerExecutionType;

public class SearchRequirementsFixture implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(SearchRequirements.class).addTemplate("new", new Rule() {{
            add("searchActionType", SearchActionType.SUBREDDIT);
            add("subReddits", new String[]{"cats", "dogs"});
            add("crawlerExecType", SubRedditCrawlerExecutionType.SYNC);
        }});

        Fixture.of(SearchRequirements.class).addTemplate("sync", new Rule() {{
            add("searchActionType", SearchActionType.SUBREDDIT);
            add("subReddits", new String[]{"cats", "dogs"});
            add("crawlerExecType", SubRedditCrawlerExecutionType.SYNC);
        }});

        Fixture.of(SearchRequirements.class).addTemplate("async", new Rule() {{
            add("searchActionType", SearchActionType.SUBREDDIT);
            add("subReddits", new String[]{"cats", "dogs"});
            add("crawlerExecType", SubRedditCrawlerExecutionType.ASYNC);
        }});
    }

}
