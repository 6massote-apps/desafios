package com.crawler.redditbot.fixture;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.Rule;
import br.com.six2six.fixturefactory.loader.TemplateLoader;
import com.crawler.redditbot.domain.model.NotificationRequirements;

public class NotificationRequirementsFixture implements TemplateLoader {

    @Override
    public void load() {
        Fixture.of(NotificationRequirements.class).addTemplate("new", new Rule() {{
            add("telegramChatId", 476304284l);
        }});
    }

}
