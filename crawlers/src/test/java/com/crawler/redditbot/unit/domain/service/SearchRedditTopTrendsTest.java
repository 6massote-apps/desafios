package com.crawler.redditbot.unit.domain.service;

import br.com.six2six.fixturefactory.Fixture;
import br.com.six2six.fixturefactory.loader.FixtureFactoryLoader;
import com.crawler.redditbot.domain.notifier.TelegramNotifier;
import com.crawler.redditbot.domain.service.SubRedditCrawlerExecutor;
import com.crawler.redditbot.domain.model.NotificationRequirements;
import com.crawler.redditbot.domain.model.SearchRequirements;
import com.crawler.redditbot.domain.service.SearchRedditTopTrends;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.mockito.Mockito.verify;

@RunWith(SpringRunner.class)
@SpringBootTest
public class SearchRedditTopTrendsTest {

    @Mock
    private SubRedditCrawlerExecutor subRedditCrawlerExecutorMock;

    @Mock
    private TelegramNotifier telegramNotifierMock;

    @Autowired
    @InjectMocks
    private SearchRedditTopTrends searchRedditTopTrends;

    @BeforeClass
    public static void setUpAll() {
        FixtureFactoryLoader.loadTemplates("com.crawler.redditbot");
    }

    @Test
    public void search() {
        SearchRequirements searchRequirements =  Fixture.from(SearchRequirements.class).gimme("sync");
        NotificationRequirements notificationRequirements =  Fixture.from(NotificationRequirements.class).gimme("new");

        searchRedditTopTrends.search(searchRequirements, notificationRequirements);

        verify(subRedditCrawlerExecutorMock).executeSync(searchRequirements, notificationRequirements);
        verify(telegramNotifierMock).notifySearchStart(searchRequirements, notificationRequirements);
    }
}
