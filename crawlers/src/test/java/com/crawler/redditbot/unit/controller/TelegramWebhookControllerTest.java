package com.crawler.redditbot.unit.controller;

import com.crawler.redditbot.RedditBotCrawlerApplication;
import com.crawler.redditbot.controller.webhook.telegram.TelegramWebhookController;
import com.crawler.redditbot.controller.webhook.telegram.dto.TelegramWebhookRequest;
import com.crawler.redditbot.domain.model.SearchRequirements;
import com.crawler.redditbot.domain.service.SearchRedditTopTrends;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.io.File;

import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(TelegramWebhookController.class)
public class TelegramWebhookControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private SearchRedditTopTrends searchRedditTopTrends;

    @Test
    public void create() throws Exception {
        String jsonPath = ClassLoader.getSystemResource("payloads/telegram_webhook_message_payload.json").getFile();

        TelegramWebhookRequest telegramWebhookRequest = objectMapper.readValue(new File(jsonPath), TelegramWebhookRequest.class);

        mockMvc.perform(post("/api/webhooks/telegram")
                .header("Content-Type", "application/json")
                .content(objectMapper.writeValueAsString(telegramWebhookRequest)))
                .andExpect(content().contentType("application/json"))
                .andExpect(status().is2xxSuccessful());

        verify(searchRedditTopTrends).search(telegramWebhookRequest.buildSearchRequirements(), telegramWebhookRequest.buildNotificationRequirements());
    }


}
