package com.crawler.redditbot.unit.controller.webhook.telegram.dto;

import com.crawler.redditbot.controller.webhook.telegram.dto.TelegramWebhookRequest;
import com.crawler.redditbot.domain.model.NotificationRequirements;
import com.crawler.redditbot.domain.model.SearchActionType;
import com.crawler.redditbot.domain.model.SearchRequirements;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.IOException;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
public class TelegramWebhookRequestTest {

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    public void givenRequest_buildSearchRequirements() throws IOException {
        String jsonPath = ClassLoader.getSystemResource("payloads/telegram_webhook_message_payload.json").getFile();

        TelegramWebhookRequest telegramWebhookRequest = objectMapper.readValue(new File(jsonPath), TelegramWebhookRequest.class);

        SearchRequirements searchRequirements = telegramWebhookRequest.buildSearchRequirements();

        assertThat(searchRequirements).isNotNull();
        assertThat(searchRequirements.getSearchActionType()).isEqualTo(SearchActionType.SUBREDDIT);
        assertThat(searchRequirements.getSubReddits()).isEqualTo(new String[] {"cats", "dogs"});
    }

    @Test
    public void givenRequest_buildNotificationRequirements() throws IOException {
        String jsonPath = ClassLoader.getSystemResource("payloads/telegram_webhook_message_payload.json").getFile();

        TelegramWebhookRequest telegramWebhookRequest = objectMapper.readValue(new File(jsonPath), TelegramWebhookRequest.class);

        NotificationRequirements notificationRequirements = telegramWebhookRequest.buildNotificationRequirements();

        assertThat(notificationRequirements).isNotNull();
        assertThat(notificationRequirements.getTelegramChatId()).isEqualTo(476304284);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenEmptyText_ThrowSomesErrors() throws IOException {
        TelegramWebhookRequest telegramWebhookRequest = new TelegramWebhookRequest();

        telegramWebhookRequest.buildSearchRequirements();
    }

    @Test
    public void givenNotFormattedText_ThrowSomesErrors() throws IOException {
        String jsonPath = ClassLoader.getSystemResource("payloads/telegram_webhook_message_unformatted_payload.json").getFile();

        TelegramWebhookRequest telegramWebhookRequest = objectMapper.readValue(new File(jsonPath), TelegramWebhookRequest.class);

        SearchRequirements searchRequirements = telegramWebhookRequest.buildSearchRequirements();

        assertThat(searchRequirements).isNotNull();
        assertThat(searchRequirements.getSearchActionType()).isEqualTo(SearchActionType.SUBREDDIT);
        assertThat(searchRequirements.getSubReddits()).isEqualTo(new String[] {"cats", "dogs"});
    }
}
