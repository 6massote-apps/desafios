
package com.crawler.redditbot.controller.webhook.telegram.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class From {

    @JsonProperty("last_name")
    public String lastName;
    public Long id;
    @JsonProperty("first_name")
    public String firstName;

}
