
package com.crawler.redditbot.controller.webhook.telegram.dto;


import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Chat {

    @JsonProperty("last_name")
    public String lastName;
    @NotNull
    public Long id;
    public String type;
    @JsonProperty("first_name")
    public String firstName;

}
