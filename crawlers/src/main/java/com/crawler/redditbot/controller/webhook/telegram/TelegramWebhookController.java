package com.crawler.redditbot.controller.webhook.telegram;

import com.crawler.redditbot.controller.webhook.telegram.dto.TelegramWebhookRequest;
import com.crawler.redditbot.domain.service.SearchRedditTopTrends;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;

@Log
@RequestMapping(
        value = "/api/webhooks/telegram",
        produces = {"application/json"},
        consumes = {"application/json"})
@Controller
public class TelegramWebhookController {

    @Autowired
    private SearchRedditTopTrends searchRedditTopTrends;

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity create(@Valid @RequestBody final TelegramWebhookRequest request) {
        log.info(String.format("Received webhook=%s of type=%s from TelegramBot...", request, request.getType()));

        searchRedditTopTrends.search(request.buildSearchRequirements(), request.buildNotificationRequirements());

        return ResponseEntity
                .noContent()
                .header("Content-Type", MediaType.APPLICATION_JSON.toString())
                .build();
    }

}
