
package com.crawler.redditbot.controller.webhook.telegram.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.ToString;

import javax.validation.constraints.NotNull;

@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Message {

    public Long date;
    @NotNull
    public Chat chat;
    @JsonProperty("message_id")
    public Long messageId;
    public From from;
    @NotNull
    public String text;

}
