package com.crawler.redditbot.controller.webhook.telegram.dto;


import com.crawler.redditbot.domain.model.NotificationRequirements;
import com.crawler.redditbot.domain.model.SearchActionType;
import com.crawler.redditbot.domain.model.SearchRequirements;
import com.crawler.redditbot.domain.model.SubRedditCrawlerExecutionType;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.util.StringUtils;

import javax.validation.constraints.NotNull;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class TelegramWebhookRequest {

    @JsonProperty("update_id")
    private Long updateId;
    @NotNull
    private Message message;
    private String execType;

    public boolean isMessage() {
        return message != null ? true : false;
    }

    public Object getType() {
        if (isMessage())
            return "message";
        return "inlineQuery";
    }

    public SearchRequirements buildSearchRequirements() {
        SearchActionType searchActionType = null;

        if (message == null || message.text == null) {
            throw new IllegalArgumentException("Text parameter required to build searchRequirements.");
        }

        String action = message.text.replaceAll("@RedditTrendsBot", "");
        String[] textRequested = action.trim().split("\\s+");

        if (textRequested.length < 2) {
            throw new IllegalArgumentException(String.format("Action require at least 2 arguments [action subreddits] for text=%s", message.text));
        }

        if (!textRequested[0].contains(SearchActionType.SUBREDDIT.getType())) {
            throw new IllegalArgumentException(String.format("Action=%s not permitted for text=%s", textRequested[0], message.text));
        }
        searchActionType = SearchActionType.SUBREDDIT;

        String[] subReddits = textRequested[1].split(";");

        if (subReddits.length < 1) {
            throw new IllegalArgumentException(String.format("Action require at least 1 subreddit for text=%s", message.text));
        }

        SubRedditCrawlerExecutionType crawlerExecType = SubRedditCrawlerExecutionType.ASYNC;
        if (execType != null) {
            if (SubRedditCrawlerExecutionType.SYNC.toString().toLowerCase().equals(execType)
                    || SubRedditCrawlerExecutionType.ASYNC.toString().toLowerCase().equals(execType)) {
                crawlerExecType = SubRedditCrawlerExecutionType.valueOf(execType.toUpperCase());
            }
        }

        return SearchRequirements.builder()
                .crawlerExecType(crawlerExecType)
                .searchActionType(searchActionType)
                .subReddits(subReddits)
                .build();
    }

    public NotificationRequirements buildNotificationRequirements() {
        if (message == null
                || message.chat == null
                || message.chat.id == null
                || StringUtils.isEmpty(message.chat.id)) {
            throw new IllegalArgumentException("ChatId parameter required to build notificationRequirements.");
        }

        return NotificationRequirements.builder()
                .telegramChatId(message.chat.id)
                .build();
    }
}
