package com.crawler.redditbot.config;

import lombok.Getter;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SubRedditCrawlerConfig {

    @Getter
    @Value("${crawler.webdriver.chrome.driver:/usr/local/chromedriver}")
    private String driverPath;
    @Getter
    @Value("${crawler.subreddit.baseUrl}")
    private String url;
    @Getter
    @Value("${crawler.limitOfPages:3}")
    private int limitOfPages;

    public static int BASE_SCORE;

    @Bean
    public DesiredCapabilities desiredCapabilities() {
        DesiredCapabilities capabilities = DesiredCapabilities.chrome();

        ChromeOptions options = new ChromeOptions();
        options.addArguments("headless");
        options.addArguments("window-size=1200x600");

        capabilities.setCapability(ChromeOptions.CAPABILITY, options);

        return capabilities;
    }

    @Value("${crawler.base.trend.score:5000}")
    public void setDatabase(final int baseScore) {
        BASE_SCORE = baseScore;
    }

}
