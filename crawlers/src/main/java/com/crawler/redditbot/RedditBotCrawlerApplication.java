package com.crawler.redditbot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@EnableAsync
@Controller
@SpringBootApplication
public class RedditBotCrawlerApplication {

	@RequestMapping("/")
	@ResponseBody
	String home() {
		return "Hello Crawler!";
	}

	public static void main(String[] args) {
		SpringApplication.run(RedditBotCrawlerApplication.class, args);
	}
}
