package com.crawler.redditbot.domain.notifier;

import com.crawler.redditbot.client.telegram.TelegramClientApi;
import com.crawler.redditbot.client.telegram.TelegramMessagePayload;
import com.crawler.redditbot.domain.model.NotificationRequirements;
import com.crawler.redditbot.domain.model.SearchRequirements;
import com.crawler.redditbot.domain.model.SubRedditThread;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Log
@Service
public class TelegramNotifier {

    @Value("${messages.subreddit.trends.search}")
    private String searchSubRedditTrendsMessage;

    @Autowired
    private TelegramClientApi telegramClientApi;

    public void notifySearchStart(SearchRequirements searchRequirements, NotificationRequirements notificationRequirements) {
        final TelegramMessagePayload telegramMessagePayload = TelegramMessagePayload.builder()
                .chat_id(notificationRequirements.getTelegramChatId())
                .text(String.format(searchSubRedditTrendsMessage, searchRequirements.getSubReddits()))
                .parse_mode("HTML")
                .build();

        telegramClientApi.sendMessage(telegramMessagePayload);
        log.info(String.format("Sent client notification=%s about starting search process.", telegramMessagePayload));
    }

    public void sendSubRedditMessage(NotificationRequirements notificationRequirements, SubRedditThread subRedditThread) {
        final String telegramText = String.format("<b>%s: </b><a href=\"%s\">%s</a>",
                subRedditThread.getSubReddit().getTitle(),
                subRedditThread.getCommentsUrl(),
                subRedditThread.getTitle());

        TelegramMessagePayload telegramMessagePayload = TelegramMessagePayload.builder()
                .chat_id(notificationRequirements.getTelegramChatId())
                .text(telegramText)
                .parse_mode("HTML")
                .build();

        telegramClientApi.sendMessage(telegramMessagePayload);
        log.info(String.format("Sent client notification=%s about top trends subReddits.", telegramMessagePayload));
    }

    public void sendSubRedditNotFountMessage(NotificationRequirements notificationRequirements, String subRedditName) {
        final String telegramText = String.format("<b>%s: </b> People don't like to talk so much about it :/",
                subRedditName);

        TelegramMessagePayload telegramMessagePayload = TelegramMessagePayload.builder()
                .chat_id(notificationRequirements.getTelegramChatId())
                .text(telegramText)
                .parse_mode("HTML")
                .build();

        telegramClientApi.sendMessage(telegramMessagePayload);
        log.info(String.format("Sent client notification=%s about top trends subReddits.", telegramMessagePayload));
    }

}
