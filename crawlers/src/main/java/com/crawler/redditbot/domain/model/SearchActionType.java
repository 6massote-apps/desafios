package com.crawler.redditbot.domain.model;

import lombok.Getter;

public enum SearchActionType {
    SUBREDDIT("subreddit");

    @Getter
    private String type;

    SearchActionType(String type) {
        this.type = type;
    }
}
