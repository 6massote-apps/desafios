package com.crawler.redditbot.domain.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class SearchRequirements {

    // crawler info
    private SearchActionType searchActionType;
    private String[] subReddits;

    // Execution Type
    private SubRedditCrawlerExecutionType crawlerExecType;

}
