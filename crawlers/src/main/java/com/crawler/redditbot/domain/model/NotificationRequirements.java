package com.crawler.redditbot.domain.model;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class NotificationRequirements {

    private Long telegramChatId;

}
