package com.crawler.redditbot.domain.model;

public enum TelegramNotificationType {
    MESSAGE("message"), INLINE_QUERY("inline_query");

    private String type;

    TelegramNotificationType(String type) {
        this.type = type;
    }
}
