package com.crawler.redditbot.domain.service;

import com.crawler.redditbot.config.SubRedditCrawlerConfig;
import com.crawler.redditbot.domain.model.SubReddit;
import com.crawler.redditbot.domain.model.SubRedditThread;
import lombok.extern.java.Log;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.CompletableFuture;

/**
 * Crawling SubReddits looking for trend threads.
 *
 * @author 6massote
 */
@Log
@Component
public class SubRedditCrawler {

    @Autowired
    private SubRedditCrawlerConfig config;

    @Autowired
    private DesiredCapabilities capabilities;

    public SubRedditThread process(final String subRedditName) {
        final Map<Long, SubRedditThread> threadTrends = new TreeMap<>(Collections.reverseOrder());

        int pageNumber = 0;

        final String url = String.format(config.getUrl(), subRedditName);
        log.info(String.format("Init process for subreddit=%s using url=%s", subRedditName, url));

        final WebDriver driver = new ChromeDriver(capabilities);

        // Navigate to URL
        driver.get(url);

        threadTrends.putAll(proccessPage(driver, pageNumber));
        pageNumber++;

        while (pageNumber < config.getLimitOfPages()) {
            List<WebElement> nextPageElement = driver.findElements(By.xpath("//*[@id=\"siteTable\"]/div/span/span[@class='next-button']/a"));

            if (nextPageElement == null || nextPageElement.isEmpty()) {
                break;
            }

            driver.get(nextPageElement.get(0).getAttribute("href"));

            threadTrends.putAll(proccessPage(driver, pageNumber));
            pageNumber++;
        }

        // Close driver
        driver.quit();

        log.info(String.format("Crawler finished for subreddit=%s with %s results.", subRedditName, threadTrends.size()));

        // Return only de best
        if (threadTrends.entrySet().iterator().hasNext()) {
            SubRedditThread subRedditThread = threadTrends.entrySet().iterator().next().getValue();
            return subRedditThread;
        } else {
            return null;
        }

    }

    private static Map<Long, SubRedditThread> proccessPage(final WebDriver driver, int pageNumber) {
        log.info(String.format("Processing page=%s using url=%s", pageNumber, driver.getCurrentUrl()));

        // validate if page is a reddit page
        List<WebElement> noContentElement = driver.findElements(By.xpath("//*[@id=\"noresults\"]"));
        if (noContentElement != null && !noContentElement.isEmpty()) {
            log.warning(String.format("Process aborted for page=%s using url=%s. Error=Page without results.", pageNumber, driver.getCurrentUrl()));
            return Collections.emptyMap();
        }

        List<WebElement> threads = driver.findElements(By.xpath(String.format("//div[@id=\"siteTable\"]" +
                "/div[contains(@class, ' thing ') " +
                "and @data-score " +
                "and number(@data-score) >= %s " +
                "and @data-context]", SubRedditCrawlerConfig.BASE_SCORE)));

        if (threads.isEmpty()) {
            log.warning(String.format("Process aborted for page=%s using url=%s. Error=Threads elements not found.", pageNumber, driver.getCurrentUrl()));
            return Collections.emptyMap();
        }

        // Using map for insert ordered.
        final Map<Long, SubRedditThread> subRedditThreads = new TreeMap<>();
        threads.stream()
                .map(t -> {
                    try {
                        String rank = t.findElement(By.xpath("span[@class='rank']")).getText();

                        if (StringUtils.isEmpty(rank)) {
                            return SubRedditThread.builder().build();
                        }

                        String score = t.getAttribute("data-score");
                        String imageUrl = t.getAttribute("data-url");
                        String title = t.findElement(By.xpath("div[2]/div[1]/p[1]/a")).getText();
                        String commentsUrl = t.findElement(By.xpath("div[2]/div[1]/ul/li[1]/a")).getAttribute("href");

                        SubReddit subReddit = SubReddit.builder()
                                .title(driver.getTitle())
                                .url(driver.getCurrentUrl())
                                .build();

                        return SubRedditThread.builder()
                                .subReddit(subReddit)
                                .title(title)
                                .rank(rank)
                                .score(score)
                                .imageUrl(imageUrl)
                                .url(commentsUrl)
                                .commentsUrl(commentsUrl)
                                .build();
                    } catch (Exception ex) {
                        log.warning(String.format("Something goes wrong processing element=%s.", t));
                        ex.printStackTrace();

                        return SubRedditThread.builder().build();
                    }
                })
                .filter(t -> {
                    if (StringUtils.isEmpty(t.getRank()) || StringUtils.isEmpty(t.getScore()))
                        return false;

                    return Integer.valueOf(t.getScore()) >= SubRedditCrawlerConfig.BASE_SCORE;
                }).forEach(subReddit -> subRedditThreads.put(Long.valueOf(subReddit.getScore()), subReddit));

        return subRedditThreads;
    }

}
