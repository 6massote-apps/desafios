package com.crawler.redditbot.domain.service;

import com.crawler.redditbot.domain.model.NotificationRequirements;
import com.crawler.redditbot.domain.model.SearchRequirements;
import com.crawler.redditbot.domain.model.SubRedditCrawlerExecutionType;
import com.crawler.redditbot.domain.model.SubRedditThread;
import com.crawler.redditbot.domain.notifier.TelegramNotifier;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Log
@Service
public class SubRedditCrawlerExecutor {

    @Autowired
    private ApplicationContext cox;

    @Autowired
    private TelegramNotifier telegramNotifier;

    @Autowired
    private SubRedditCrawler subRedditCrawler;

    @Async
    public void executeAsync(SearchRequirements searchRequirements, NotificationRequirements notificationRequirements) {
        executeSync(searchRequirements, notificationRequirements);
    }

    public void executeSync(SearchRequirements searchRequirements, NotificationRequirements notificationRequirements) {
        int numberOfReddits = searchRequirements.getSubReddits().length;

        log.info(String.format("Starting processing for %d subReddits crawlers using %s way...", numberOfReddits, searchRequirements.getCrawlerExecType()));

        final String[] subReddits = searchRequirements.getSubReddits();

        for (int i=0; i<numberOfReddits; i++) {
            String subRedditName = subReddits[i];

            SubRedditThread subRedditThread = subRedditCrawler.process(subRedditName);

            if (subRedditThread == null) {
                telegramNotifier.sendSubRedditNotFountMessage(notificationRequirements, subRedditName);
                continue;
            }

            telegramNotifier.sendSubRedditMessage(notificationRequirements, subRedditThread);
        }
    }
}
