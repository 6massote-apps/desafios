package com.crawler.redditbot.domain.service;

import com.crawler.redditbot.client.telegram.TelegramClientApi;
import com.crawler.redditbot.client.telegram.TelegramMessagePayload;
import com.crawler.redditbot.domain.model.NotificationRequirements;
import com.crawler.redditbot.domain.model.SearchRequirements;
import com.crawler.redditbot.domain.model.SubRedditCrawlerExecutionType;
import com.crawler.redditbot.domain.notifier.TelegramNotifier;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Log
@Service
public class SearchRedditTopTrends {

    @Autowired
    private SubRedditCrawlerExecutor subRedditCrawlerExecutor;

    @Autowired
    private TelegramNotifier telegramNotifier;

    public void search(final SearchRequirements searchRequirements, final NotificationRequirements notificationRequirements) {
        telegramNotifier.notifySearchStart(searchRequirements, notificationRequirements);

        if (SubRedditCrawlerExecutionType.SYNC.equals(searchRequirements.getCrawlerExecType())) {
            subRedditCrawlerExecutor.executeSync(searchRequirements, notificationRequirements);
        } else {
            subRedditCrawlerExecutor.executeAsync(searchRequirements, notificationRequirements);
        }
    }

}
