package com.crawler.redditbot.domain.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SubRedditThread {

    private SubReddit subReddit;

    private String title;
    private String rank;
    private String score;
    private String imageUrl;
    private String url;
    private String commentsUrl;

}
