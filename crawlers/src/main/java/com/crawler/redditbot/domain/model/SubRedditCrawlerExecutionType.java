package com.crawler.redditbot.domain.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum SubRedditCrawlerExecutionType {
    ASYNC("async"), SYNC("sync");

    private String type;
}
