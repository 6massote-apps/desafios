package com.crawler.redditbot.domain.model;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class SubReddit {

    private String title;
    private String url;

}
