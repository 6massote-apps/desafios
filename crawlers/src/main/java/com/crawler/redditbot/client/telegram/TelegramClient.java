package com.crawler.redditbot.client.telegram;

import feign.Feign;
import feign.Logger;
import feign.gson.GsonDecoder;
import feign.gson.GsonEncoder;
import feign.okhttp.OkHttpClient;
import feign.slf4j.Slf4jLogger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Configuration
public class TelegramClient {

    @Value("${clients.telegram.api.endpoint}")
    private String telegramApiEndpoint;

    @Value("${clients.telegram.api.token}")
    private String telegramApiToken;

    @Bean
    public TelegramClientApi telegramClientApi() {
        return Feign.builder()
                .client(new OkHttpClient())
                .encoder(new GsonEncoder())
                .decoder(new GsonDecoder())
                .logger(new Slf4jLogger(TelegramClientApi.class))
                .logLevel(Logger.Level.FULL)
                .target(TelegramClientApi.class, String.format(telegramApiEndpoint, telegramApiToken));
    }
}
