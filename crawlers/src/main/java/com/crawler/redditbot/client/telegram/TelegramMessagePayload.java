package com.crawler.redditbot.client.telegram;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.ToString;

@ToString
@Builder
public class TelegramMessagePayload {

    @JsonProperty("chat_id")
    public long chat_id;
    public String text;
    @JsonProperty("parse_mode")
    public String parse_mode;

}
