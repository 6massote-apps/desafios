package com.crawler.redditbot.client.telegram;

import feign.Headers;
import feign.RequestLine;

public interface TelegramClientApi {

    @RequestLine("POST /sendMessage")
    @Headers("Content-Type: application/json")
    void sendMessage(final TelegramMessagePayload telegramMessagePayload);

}
