Esta aplicação contempla os requisitos descritos no tópico Desafio 2: Crawlers, pode ser executado apenas com a utilização do Docker e ser feita a validação live com o Bot do Telegram @RedditTrendsBot.

Abaixos possuímos algumas instruções e variações de como executar o projeto. Assim como a `estratégia e arquitetura adotada`, `sugestões`, `build e run local` e `como fazer o teste live da aplicação`.

#### Estatégia

Para o desenvolvimento do crawler e sua utilização direta com o Bot do Telegram, foi utilizada a API de WebHooks do Telegram disponibilizada para nesta página: https://core.telegram.org/bots/webhooks.
A opção de WebHook foi escolhida pelo fato de não desenvolvermos uma estratégia de Pooling na API do Telegram. Com WebHooks seremos notificados apenas quando tivermos notificações em nosso Bot.

Qual o nome do Bot? `@RedditTrendsBot`. 


Para instala-lo, basta procura-lo no Telegram. 

E como solicitar Top Trends ou a lista de *threads* que estão bombando no Reddit naquele momento?


Dentro do Bot utilize a action `/subreddits askreddit;worldnews;cats`. Ao solicitar as trends para o Bot, iremos notificar ondemand as threads encontradas.
Retornaremos apenas `1 por subreddit` e quando não tivermos algum resultado para algum subreddit, iremos informar o usuário.

#### Arquitetura e fluxo do usuário

Foi utilizado Springboot para o desenvolvimento da API que recebe o WebHook e SeleniumHQ para o crawler. Utilizamos também o Gitlab para Continuous Integration e Deployment no Heroku.

![](docs/redditbot.jpeg)

Explicando um pouco da imagem acima:
- Ao receber uma requisição do WebHook do Telegram, avisaremos o usuário que estamos processando sua solicitação.
- Após a aplicação identificar as subreddits para fazer sua análise, iremos processar de forma assíncrona em uma thread apartada da thread que fez a requisição.
- Terminando o processamento de cada subreddits, a aplicação avisa o usuário se encontramos algum top trend ou não.

A aplicação possui algumas parametrizações importantes:
- Quantidade de páginas que iremos buscar por topTrends: 3
- Padrão de URL que iremos utilizar do Reddit: https://www.reddit.com/r/%s
- Configuração da API do Reddit: https://api.telegram.org/bot%s
- Score base que consideramos ser um Top Trend ou não: 5000

#### Testando live

- Acessar o bot `@RedditTrendsBot` no aplicativo ou página web.
- Enviar o comando `/subreddits askreddit;worldnews;cats`
- Aguardar a resposta com sugestões de links

Processo pode demorar devido a capacidade de processador e memória das máquinas no servidor :)

#### Build e run da aplicação localmente

Possuímos um script para facilitar a execução simulando o que o GitLab CI faz.

```bash
$ ./build.sh
```
Passos efetuados pelo script:
- Build da aplicação em um container de build.
- Teste unitário e depois de integração.
- Copia do .jar gerado para um container de run

É possível ver os mesmos passos no Pipeline do Gitlab CI: https://gitlab.com/6massote-apps/desafios/pipelines

Os testes de integração utilizam mocks em todas as fronteiras:

![](docs/redditbot_tests.jpeg)

Para os testes de integração, nos baseamos em algumas documentações e mocks:
- Request API Telegram WebHook para nossa aplicação: nos baseamos na doc do prórpio Telegram onde eles exemplificam os contratos para a chamada do WebHooks.
- Request aplicação para API de Bot Telegram: nos baseamos na Doc de todas as operações suportadas pela API de Bots.
- Páginas dos SubReddits: foi feito export da alguns exemplos de HTML para rodarmos o crawler sem precisar acessar a página via internet.

Estes mocks podem ser encontrados na pasta `resources`. 

#### Para executar a aplicação, rode o comando abaixo:

Será dificil simular o cenário de um WebHook rodando localmente, por isso a importancia dos testes de integração. Para testar Live precisamos de um `chat_id` valido do Telegram. Mas abaixo, segue o comando e um payload que podemos utilizar para testar a aplicação:

```bash
$ run -ti -p 8080:8080 $DOCKER_IMAGE_RUN
```

Possuímos a seguinte API exposta para receber o WebHook:

```
[POST] http://localhost:8080/api/webhooks/telegram

Contract:
{
  "update_id": 809681988,
  "message": {
    "message_id": 20,
    "from": {
      "id": 476304284,
      "is_bot": false,
      "first_name": "Gabriel",
      "last_name": "Prado",
      "language_code": "en-US"
    },
    "chat": {
      "id": 476304284,
      "first_name": "Gabriel",
      "last_name": "Prado",
      "type": "private"
    },
    "date": 1512093898,
    "text": "/subreddit war;cats;guns",
    "entities": [
      {
        "offset": 0,
        "length": 10,
        "type": "bot_command"
      }
    ]
  }
}
```

#### Melhorias

Melhorias a serem feitas visando performance, escalabilidade e evolução da aplicação:
- Processamento utilizando alguma técnologia de mensageria para uma melhor escalabilidade no processamento de crawler de cada subreddit. Esta abordagem nos traz a possbilidade de escalar o processamento de uma forma muito fácil só aumentando nosso núemro de workers/consumers.
- Junto a uma arquitetura com mensageria, validar alteranativas de crawlers, tecnologias e libs, visando a performance na leitura dos dados da página e memória utilizada pela aplicação.
- Melhor tratamento de erros e exceptions para oferecer a melhor experiência para a interação do usuário. 

![](docs/redditbot_imp.jpeg)

#### Links úteis

- C.I e C.D: https://gitlab.com/6massote-apps/desafios/pipelines
- URL Live: https://redditbot-api.herokuapp.com/api/webhooks/telegram
- Doc Telegram: 
    - https://core.telegram.org/bots/webhooks
    - https://core.telegram.org/bots/api

Dúvidas e permissões de acesso,
email: gabrielmassote@gmail.com

# Desafio 2: Crawlers

Parte do trabalho na IDwall inclui desenvolver *crawlers/scrapers* para coletar dados de websites.
Como nós nos divertimos trabalhando, às vezes trabalhamos para nos divertir!

O Reddit é quase como um fórum com milhares de categorias diferentes. Com a sua conta, você pode navegar por assuntos técnicos, ver fotos de gatinhos, discutir questões de filosofia, aprender alguns life hacks e ficar por dentro das notícias do mundo todo!

Subreddits são como fóruns dentro do Reddit e as postagens são chamadas *threads*.

Para quem gosta de gatos, há o subreddit ["/r/cats"](https://www.reddit.com/r/cats) com threads contendo fotos de gatos fofinhos.
Para *threads* sobre o Brasil, vale a pena visitar ["/r/brazil"](https://www.reddit.com/r/brazil) ou ainda ["/r/worldnews"](https://www.reddit.com/r/worldnews/).
Um dos maiores subreddits é o "/r/AskReddit".

Cada *thread* possui uma pontuação que, simplificando, aumenta com "up votes" (tipo um like) e é reduzida com "down votes".

Sua missão é encontrar e listar as *threads* que estão bombando no Reddit naquele momento!
Consideramos como bombando *threads* com 5000 pontos ou mais.

## Entrada
- Lista com nomes de subreddits separados por ponto-e-vírgula (`;`). Ex: "askreddit;worldnews;cats"

### Parte 1
Gerar e imprimir uma lista contendo número de upvotes, subreddit, título da thread, link para os comentários da thread, link da thread.
Essa parte pode ser um CLI simples, desde que a formatação da impressão fique legível.

### Parte 2
Construir um robô que nos envie essa lista via Telegram sempre que receber o comando `/NadaPraFazer [+ Lista de subrredits]` (ex.: `/NadaPraFazer programming;dogs;brazil`)


Qualquer método para coletar os dados é válido. Caso não saiba por onde começar, procure por SeleniumHQ (Java), PhantomJS (Javascript) e Scrapy (Python).
