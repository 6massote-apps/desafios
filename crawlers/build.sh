#!/bin/bash
set -e

export VERSION=1.0.0
export DOCKER_IMAGE_BUILD=redditbot-build:$VERSION
export DOCKER_IMAGE_RUN=redditbot-run:$VERSION

echo "#### Building project inside docker container..."
docker build -f Dockerfile.build -t $DOCKER_IMAGE_BUILD .
echo "#### Running unit tests..."
docker run $DOCKER_IMAGE_BUILD
echo "#### Running integration tests..."
docker run $DOCKER_IMAGE_BUILD mvn clean package -Dspring.profiles.active=test
echo "#### Extractig .jar file from build container..."
time docker run $DOCKER_IMAGE_BUILD cat target/redditbot.jar > redditbot.jar
echo "#### Copy .jar file from build container to run container..."
docker build -t $DOCKER_IMAGE_RUN .
echo "#### Validating build date for .jar file:"
docker run $DOCKER_IMAGE_RUN ls -lha redditbot.jar
