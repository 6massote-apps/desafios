package com.algorithm;

import org.junit.Test;

import static org.junit.Assert.*;

public class TextFormatterAppTest {

    @Test(expected = IllegalArgumentException.class)
    public void givenMain_withNullArgs() throws Exception {
        TextFormatterApp.main(null);

    }

    @Test(expected = IllegalArgumentException.class)
    public void givenMain_withOneArgs() throws Exception {
        TextFormatterApp.main(new String[]{"lineLimit=20"});

    }

    @Test
    public void givenMain_withTwoArgs() throws Exception {
        TextFormatterApp.main(new String[]{"file=/tmp/text.txt", "lineLimit=40"});

    }

    @Test
    public void givenMain_withThreeArgs() throws Exception {
        TextFormatterApp.main(new String[]{"file=/tmp/text.txt", "lineLimit=40", "justify=true"});
    }

    @Test
    public void givenMain_withManyLineLimitsArgs() throws Exception {
        for(int i=30; i<100; i++) {
            System.out.println("Index:"+i);
            TextFormatterApp.main(new String[]{"file=/tmp/text.txt", "lineLimit="+i, "justify=true"});
        }
    }
}