package com.algorithm.strings;

import com.algorithm.utils.TestCaseReader;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TextFormatterTest {

    @Test
    public void givenTextWithOneParagraphs_formatLinesWithLimitOfChars() throws Exception {
        final TextFormatter textFormatter = new TextFormatter(
                FormatInfo.builder().
                        limitOfChars(40)
                        .build());

        Text text = TestCaseReader.readFromClasspath("testcase_1/in.txt");
        Text textExpected = TestCaseReader.readFromClasspath("testcase_1/out.txt");

        Text textFormatted = textFormatter.format(text);

        assertEquals(textExpected.getContent(), textFormatted.getContent());
    }

    @Test
    public void givenTextWithTwoParagraphs_formatLinesWithLimitOfChars() throws Exception {
        final TextFormatter textFormatter = new TextFormatter(
                FormatInfo.builder().
                        limitOfChars(40)
                        .build());

        Text text = TestCaseReader.readFromClasspath("testcase_2/in.txt");
        Text textExpected = TestCaseReader.readFromClasspath("testcase_2/out.txt");

        Text textFormatted = textFormatter.format(text);

        assertEquals(textExpected.getContent(), textFormatted.getContent());
    }

    @Test
    public void givenTextWithTwoParagraphs_formatWithJustify() throws Exception {
        final TextFormatter textFormatter = new TextFormatter(
                FormatInfo.builder().
                        limitOfChars(40)
                        .textJustifier(new TextJustifier(40))
                        .build());

        Text text = TestCaseReader.readFromClasspath("testcase_3/in.txt");
        Text textExpected = TestCaseReader.readFromClasspath("testcase_3/out.txt");

        Text textFormatted = textFormatter.format(text);

        assertEquals(textExpected.getContent(), textFormatted.getContent());
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenEmptyInput_throwIllegalArgumentException() throws Exception {
        new TextFormatter(null).format(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenEmptyFormatInfo_throwIllegalArgumentException() throws Exception {
        new TextFormatter(null).format(Text.builder().content("Teste").build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenEmptyLimitOfChars_throwIllegalArgumentException() throws Exception {
        new TextFormatter(FormatInfo.builder().build()).format(Text.builder().content("Teste").build());
    }

    @Test(expected = IllegalArgumentException.class)
    public void givenLimitOfCharsLower30_throwIllegalArgumentException() throws Exception {
        new TextFormatter(FormatInfo.builder()
                .limitOfChars(10)
                .build())
            .format(Text.builder().content("Teste").build());
    }

    @Test
    public void givenEmptyText_throwIllegalArgumentException() throws Exception {
        Text textFormatted = new TextFormatter(FormatInfo.builder()
                .limitOfChars(40)
                .build())
            .format(Text.builder().content("").build());

        assertEquals("", textFormatted.getContent());
    }

}
