package com.algorithm.strings;

import org.junit.Test;

import static org.junit.Assert.*;

public class TextTest {

    @Test
    public void builder() {
        Text text = Text.builder()
                .content("Ola, tudo bem?")
                .build();

        assertNotNull(text);
        assertNotNull(text.getContent());
        assertEquals(14, text.getContent().length());
    }

}