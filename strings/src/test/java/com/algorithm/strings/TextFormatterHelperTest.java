package com.algorithm.strings;

import org.junit.Test;

import static org.junit.Assert.*;

public class TextFormatterHelperTest {

    @Test
    public void findNextWord() {
        String text = new String("In a \"beginning\" God created the heavens and the earth. waters.");

        assertEquals(TextFormatterHelper.findNextWord(0, text.toCharArray()), "In");
        assertEquals(TextFormatterHelper.findNextWord(3, text.toCharArray()), "a");
        assertEquals(TextFormatterHelper.findNextWord(56, text.toCharArray()), "waters.");
    }

    @Test
    public void hasExcededLineLimit() throws Exception {
        StringBuilder line7 = new StringBuilder("1234567");
        String word3 = new String("Ola");

        assertFalse(TextFormatterHelper.hasExcededLineLimit(word3, line7, 11));
        assertFalse(TextFormatterHelper.hasExcededLineLimit(word3, line7, 10));
        assertTrue(TextFormatterHelper.hasExcededLineLimit(word3, line7, 9));
    }

    @Test
    public void isSpace() throws Exception {
        assertTrue(TextFormatterHelper.isSpace(' '));
        assertFalse(TextFormatterHelper.isSpace('a'));
    }

    @Test
    public void isTabChars() throws Exception {
        assertTrue(TextFormatterHelper.isNewLineChar('\n'));
        assertFalse(TextFormatterHelper.isNewLineChar('a'));
        assertFalse(TextFormatterHelper.isNewLineChar('9'));
        assertFalse(TextFormatterHelper.isNewLineChar(' '));
    }

    @Test
    public void isLetter() throws Exception {
        assertFalse(TextFormatterHelper.isLetter('\t'));
        assertFalse(TextFormatterHelper.isLetter('\n'));
        assertFalse(TextFormatterHelper.isLetter(' '));
        assertTrue(TextFormatterHelper.isLetter('a'));
        assertTrue(TextFormatterHelper.isLetter('9'));
        assertTrue(TextFormatterHelper.isLetter(','));
        assertTrue(TextFormatterHelper.isLetter('!'));
        assertTrue(TextFormatterHelper.isLetter('@'));
        assertTrue(TextFormatterHelper.isLetter('"'));
    }

    @Test
    public void findNumberOfWordGaps() {
        assertEquals(2, TextFormatterHelper.findNumberOfWordGaps("Ola, tudo bem? "));
        assertEquals(2, TextFormatterHelper.findNumberOfWordGaps("  Ola,         tudo     bem? "));
        assertEquals(2, TextFormatterHelper.findNumberOfWordGaps("Ola,         tudo  bem? "));
        assertEquals(3, TextFormatterHelper.findNumberOfWordGaps("Ola,         tudo  bem? Sim."));
        assertEquals(4, TextFormatterHelper.findNumberOfWordGaps("Ola,         tudo  bem? Sim, claro!"));
    }
}