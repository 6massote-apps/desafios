package com.algorithm.strings;

import org.junit.Test;

import static org.junit.Assert.*;

public class FormatInfoTest {

    @Test
    public void builder() {
        FormatInfo formatInfo = FormatInfo.builder()
                .limitOfChars(40)
                .textJustifier(new TextJustifier(40))
                .build();


        assertEquals(40, formatInfo.getLimitOfChars());
        assertNotNull(formatInfo.getTextJustifier());
    }

}