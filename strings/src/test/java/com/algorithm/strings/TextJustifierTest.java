package com.algorithm.strings;

import org.junit.Test;

import static org.junit.Assert.*;

public class TextJustifierTest {
    @Test
    public void justify() throws Exception {
        final TextJustifier justifier = new TextJustifier(40);

        assertEquals("\"day,\"   and   the  darkness  he  called",
                justifier.justify("\"day,\" and the darkness he called"));
        assertEquals("of  God  was  hovering  over the waters.",
                justifier.justify("of God was hovering over the waters."));
        assertEquals("there  was  morning  -  the  first  day.",
                justifier.justify("there was morning - the first day."));

    }

}