package com.algorithm.strings;

import lombok.AllArgsConstructor;

/**
 * Justify lines for a limit of characters
 * @author 6massote
 */
@AllArgsConstructor
public class TextJustifier {

    private int lineLimit;

    public String justify(final String line) throws Exception {


        StringBuilder lineJustified = new StringBuilder();

        String[] words = line.split("\\s+");
        if (words.length == 0) {
            throw new IllegalArgumentException(String
                    .format("At least on words is required for a line=%s", line));
        }

        if (words.length == 1) {
            return line;
        }

        int availableSpaces = lineLimit - line.length();
        int completeLoopsAroundWords = availableSpaces / (words.length - 1);
        int remainingSpacesForOneLoop = availableSpaces % (words.length - 1);

        completeLoopsAroundWords++; // for existent space before split
        for (int i=0; i<completeLoopsAroundWords; i++) {
            for (int j=0; j<words.length-1; j++) {
                words[j] = words[j].concat(" ");
            }
        }

        for (int i=0; i<remainingSpacesForOneLoop; i++) {
            words[i] = words[i].concat(" ");
        }

        for(int i=0; i<words.length; i++) {
            lineJustified.append(words[i]);
        }

        if (lineJustified.length() != lineLimit)
            throw new Exception(String.format("TextJustifier proccess has a problemd. Line=%s, LimitPerLine=%s", lineJustified.length(), lineLimit));

        return lineJustified.toString();
    }

}
