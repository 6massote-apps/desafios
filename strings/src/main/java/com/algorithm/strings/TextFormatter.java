package com.algorithm.strings;

import lombok.AllArgsConstructor;

import static com.algorithm.strings.TextFormatterHelper.*;

@AllArgsConstructor
public class TextFormatter {

    private FormatInfo formatInfo;

    public Text format(final Text textToFormat) throws Exception {
        if (textToFormat == null || formatInfo == null || formatInfo.getLimitOfChars() == 0) {
            throw new IllegalArgumentException(String
                    .format("Text or FormatInfo is required. Text=%s, FormatInfo=%s", textToFormat, formatInfo));
        }

        if (formatInfo.getLimitOfChars() < 30) {
            throw new IllegalArgumentException(String
                    .format("FormatInfo requires a limit of line upper to 30. Based on Bigest words in the world. FormatInfo=%s", formatInfo));
        }

        StringBuilder line = new StringBuilder();
        StringBuilder textFormatted = new StringBuilder();

        char[] letters = textToFormat.getContent().toCharArray();
        int letterLength = letters.length;

        int index = 0;
        while(index < letterLength) {
            char ch = letters[index];

            if (isNewLineChar(ch)) {
                flushLine(line, textFormatted, false);
                line = new StringBuilder("");

                flushChar(ch, textFormatted);
                index++;
            } else if(line.length() == 0 && Character.isWhitespace(ch)) {
                index++;
                continue;
            }

            if (isLetter(ch)) {
                String word = findNextWord(index, letters);

                if (hasExcededLineLimit(word, line, formatInfo.getLimitOfChars())) {
                    flushLine(line, textFormatted, false);
                    line = new StringBuilder("");
                    line.append(word);
                } else {
                    line.append(word);
                }

                index += word.length();
                continue;
            }

            if (!isLetter(ch)) {
                if (hasExcededLineLimit(String.valueOf(ch), line, formatInfo.getLimitOfChars())) {
                    flushLine(line, textFormatted, false);
                    line = new StringBuilder("");
                } else {
                    line.append(ch);
                }

                index++;
            }
        }

        if (line.length() > 0)
            flushLine(line, textFormatted, true);

        return Text.builder()
                .content(textFormatted.toString())
                .build();
    }

    private void flushLine(StringBuilder line, StringBuilder text, boolean isLastLine) throws Exception {
        String lineTrim = line.toString()
                .replaceAll("^\\s{1,}|\\s{1,}$", "");

        if (formatInfo.getTextJustifier() != null && lineTrim.length() > 0 && lineTrim.length() != formatInfo.getLimitOfChars()) {
            lineTrim = formatInfo.getTextJustifier().justify(lineTrim);
        }

        text.append(lineTrim);
        if (!isLastLine) text.append("\n");
    }

    private void flushChar(char ch, StringBuilder text) {
        text.append(ch);
    }

}
