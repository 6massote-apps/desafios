package com.algorithm.strings;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class FormatInfo {

    private int limitOfChars;
    private TextJustifier textJustifier;

}
