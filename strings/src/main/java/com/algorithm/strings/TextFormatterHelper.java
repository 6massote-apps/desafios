package com.algorithm.strings;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextFormatterHelper {

    public static String findNextWord(final int index, final char[] letters) {
        StringBuilder word = new StringBuilder("");

        for (int i=index; i<letters.length; i++) {
            char ch = letters[i];
            if (!isLetter(ch)) break;

            word.append(ch);
        }

        return word.toString();
    }

    public static boolean isLetter(final char ch) {
        return Character.isAlphabetic(ch)
                || Character.isLetter(ch)
                || Character.isDigit(ch)
                || Character.toString(ch).matches("[^A-Za-z0-9\\s\\t\\n\\r]");
    }

    public static boolean hasExcededLineLimit(final String word, final StringBuilder line, final int limitOfChars) {
        return limitOfChars - (line.length() + word.length()) < 0;
    }

    public static boolean isSpace(final char ch) {
        return Character.isWhitespace(ch);
    }

    public static boolean isNewLineChar(final char ch) {
        return ch == '\n';
    }

    public static int findNumberOfWordGaps(final String line) {
        Pattern pattern = Pattern.compile("\\s+");
        Matcher matcher = pattern.matcher(line.trim());

        int count = 0;
        while(matcher.find()) {
            count++;
        }

        return count;
    }

}
