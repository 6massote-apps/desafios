package com.algorithm.strings;

import lombok.*;

@Builder
@Data
public class Text {

    @Setter(AccessLevel.NONE)
    private String content;

}
