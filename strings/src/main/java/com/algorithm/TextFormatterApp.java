package com.algorithm;

import com.algorithm.strings.FormatInfo;
import com.algorithm.strings.Text;
import com.algorithm.strings.TextFormatter;
import com.algorithm.strings.TextJustifier;
import com.algorithm.utils.TestCaseReader;
import java.util.HashMap;
import java.util.Map;

/**
 * TextFormatter application justify texts directly for file system. To format a specific text,
 * pass it through file param.
 *
 * This class, will generate formatted text to the output.
 *
 * Another behavior supported by this class, is to customize the file path, the limit of each line to be formatted and if we want to justify
 * or not the text file.
 *
 * We need to use following parameters:
 * - file=/tmp/text.txt
 * - lineLimit=40
 * - justify=true
 *
 * @author 6massote
 *
 */
public class TextFormatterApp {

    private static String FILE_ARG_NAME = "file";
    private static String LINE_LIMIT_ARG_NAME = "lineLimit";
    private static String JUSTIFY_ARG_NAME = "justify";

    public static void main( String[] args ) throws Exception {
        Map<String, String> paramsMap = new HashMap<>();
        int lineLimit = 40;

        if (args == null || args.length < 2) {
            throw new IllegalArgumentException("Illegal commandline arguments. App needs at least 2 args of [file, lineLimit, justify]");
        }

        try {
            for (int i = 0; i < args.length; i++) {
                String[] param = args[i].split("=");
                if (param.length == 2)
                    paramsMap.put(param[0], param[1]);
            }
        } catch (Exception e) {
            throw new IllegalArgumentException(String.format("Illegal commandline arguments. App only supports [file, lineLimit, justify] argument. Args=%s", args));
        }

        lineLimit = Integer.valueOf(paramsMap.get(LINE_LIMIT_ARG_NAME));

        final FormatInfo formatInfo = FormatInfo.builder()
                .limitOfChars(lineLimit)
                .build();

        if (paramsMap.get(JUSTIFY_ARG_NAME) != null && Boolean.valueOf(paramsMap.get(JUSTIFY_ARG_NAME))) {
            formatInfo.setTextJustifier(new TextJustifier(lineLimit));
        }
        final TextFormatter textFormatter = new TextFormatter(formatInfo);

        final Text text = TestCaseReader.readFromFileSystem(paramsMap.get(FILE_ARG_NAME));
        System.out.println(String.format("Text to format:\n%s", text.getContent()));

        final Text textFormatted = textFormatter.format(text);
        System.out.println(String.format("\n\nText formatted:\n%s", textFormatted.getContent()));
    }
}
