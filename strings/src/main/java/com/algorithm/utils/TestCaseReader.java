package com.algorithm.utils;

import com.algorithm.strings.Text;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Used to read testCase in tests scenarios.
 *
 * @author 6massote
 */
public class TestCaseReader {

    public static Text readFromClasspath(final String path) throws URISyntaxException, IOException {
        final URI uri = ClassLoader.getSystemResource(path).toURI();
        final Path testCasePath = Paths.get(uri);

        return getTextByPath(testCasePath);
    }

    public static Text readFromFileSystem(final String path) throws URISyntaxException, IOException {
        final Path testCasePath = Paths.get(path);

        return getTextByPath(testCasePath);
    }

    private static Text getTextByPath(Path testCasePath) throws IOException {
        final String string = new String(Files.readAllBytes(testCasePath));

        return Text.builder()
                .content(string)
                .build();
    }

}
