Esta aplicação contempla os requisitos descritos no tópico `Desagio 1: Strings` e pode ser executado apenas com a utilização do Docker.

Abaixos possuímos algumas instruções e variações de como executar o projeto.

# Como executar o formatador de código utilizando o Docker

Para executarmos a aplicação precisamos de ter apenas o docker instalado. O build 
e o run serão feitos dentro do próprio container.

##### Build
```bash
$ docker build -t algorithm-strings:0.0.1 .
```

Após o build do container, teremos uma imagem pronta para formatarmos os arquivos necessários.
A imagem docker está configurada com `ENTRYPOINT` para o jar gerado durante o momento de build, o que facilita
nossa vida no momento de run.

##### Run

Possuímos algumas formas de executar a aplicação:

```bash
$ docker run algorithm-strings:0.0.1
```
Desta forma, iremos utilizar um arquivo de exemplo dentro do container, localizado em /tmp/text.txt, para a formatação. Por termos 
configurado um `ENTRYPOINT` e um `CMD` na imagem do Docker, já executamos o container com alguns parâmetros padrão.

```bash
$ docker run algorithm-strings:0.0.1 file=/tmp/text.txt lineLimit=60 justify=true
```

Esta segunda forma, ainda utilizamos o mesmo arquivo dentro do container, porém podemos alterar o limite de caracteres por linha e se 
o texto deve estar justificado ou não.

```bash
$ docker run -v /path/host/text.txt:/path/container/text.txt algorithm-strings:0.0.1 file=/path/container/text.txt lineLimit=60 justify=true
```
Utilizando esta ultima forma de executarmos o container, podemos selecionar o arquivo que desejamos formatar. Para isso, devemos utilizar 
o recurso de `VOLUMES` do Docker. Ele permite que copartilhemos pasta ou arquivos do host internamente com o container.

Um ponto de atenção neste passo. Quando utilizamos a config com `VOLUMES` o parameter `file` representa um diretório e um arquivo dentro do container. O que fazemos
neste ponto é compartilhar um arquivo do nosso host com o container e especificar para a aplicação formatar o arquivo compartilhado. 
 
# Desafio 1: Strings

Após ler o coding style do kernel Linux, você descobre a mágica que é 
ter linhas de código com no máximo 80 caracteres cada uma.

Assim, você decide que de hoje em diante seus e-mails enviados também 
seguirão um padrão parecido e resolve desenvolver um plugin para te ajudar
com isso. Contudo, seu plugin aceitará no máximo 40 caracteres por linha.

Implemente uma função que receba: 
1. um texto qualquer
2. um limite de comprimento  

e seja capaz de gerar os outputs dos desafios abaixo.

## Exemplo input

`In the beginning God created the heavens and the earth. Now the earth was formless and empty, darkness was over the surface of the deep, and the Spirit of God was hovering over the waters.`

`And God said, "Let there be light," and there was light. God saw that the light was good, and he separated the light from the darkness. God called the light "day," and the darkness he called "night." And there was evening, and there was morning - the first day.`

O texto deve ser parametrizável e se quiser, pode utilizar um texto de input de sua preferência.

### Parte 1 (Básico) - limite 40 caracteres
Você deve seguir o exemplo de output [deste arquivo](https://github.com/idwall/desafios/blob/master/strings/output_parte1.txt), onde basta o texto possuir, no máximo, 40 caracteres por linha. As palavras não podem ser quebraadas no meio.

### Parte 2 (Intermediário) - limite 40 caracteres
O exemplo de output está [neste arquivo](https://github.com/idwall/desafios/blob/master/strings/output-parte2.txt), onde além de o arquivo possuir, no máximo, 40 caracteres por linha, o texto deve estar **justificado**.

### Extras

- Tratamento de erros e exceções. Fica a seu critério quais casos deseja tratar e como serão tratados.
- Testes unitários ou de integração.
- Uso do Docker.
- Parametrização da quantidade de caracteres por linha.
- Utilizar as versões mais atuais da linguagem que escolher para desenvolver (JavaScript ES6+; Java 8; Python 3, etc).
